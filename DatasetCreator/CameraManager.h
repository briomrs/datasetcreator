#pragma once
#include "FrameProvider.h"
#include "RSFrameProvider.h"
#include "Utils.h"
#include <opencv2/rgbd.hpp>
#include <chrono>
#include <thread>

namespace CV_CORE
{
	class CameraManager
	{
	private:
		FrameProvider * mFrameProvider;
		cv::Size mInfrSize;
		cv::Size mRGBSize;
		int mFps;
		CAMERATYPE mCameraType;
		cv::Mat mQMatrix;

		cv::Mat mRgbCameraMatrix;
		cv::Mat mLeftCameraMatrix;
		cv::Mat mRightCameraMatrix;
		cv::Mat mDepthCameraMatrix;
		cv::Mat mMutualRotationMatrix;
		cv::Mat mMutualTransformMatrix;
		cv::Mat mRgbDistortionMatrix;
		cv::Mat mLeftDistortionMatrix;
		cv::Mat mRightDistortionMatrix;
		cv::Mat mDepthToColorExtrinsicMat;
		//The rigid body transorm betweeen cameras. Transforms points from depth camera to rgb camera frame.
		cv::Mat mDepth2RgbRt;
		cv::Mat R1;
		cv::Mat	P1;
		cv::Mat R2;
		cv::Mat P2;
		cv::Rect roi1;
		cv::Rect roi2;

		rs2_intrinsics mDepthIntrinsics;
		rs2_intrinsics mColorIntrinsics;
		rs2_extrinsics mColorToDepthExtrinsics;
		rs2_extrinsics mDepthToColorExtrinsics;

		void InitCameraParams();
		void DefineCamera();
		void DefineCameraMatricies();
		void StereoRectify();
		bool mUseAlign;
		bool Reconnect();
		float * mCorners;
		bool mIsCornersCalculated = false;
	public:
		CameraManager(CAMERATYPE cType, bool useAlign);
		~CameraManager();

		cv::Mat GetQMatrix();
		cv::Mat GetImage(IMAGE_KIND kind);
		void Update();
		cv::Mat GetLeftCameraMatrix();
		cv::Mat GetRightCameraMatrix();
		cv::Mat GetMutualRotationMatrix();
		cv::Mat GetMutualTransformMatrix();
		cv::Mat GetLeftDistortionMatrix();
		cv::Mat GetRightDistortionMatrix();
		cv::Mat GetDepth2RgbExtrinsic();
		cv::Mat GetRgbCameraMatrix();
		cv::Mat GetAlignedDepth(const cv::Mat & depthImage);
		cv::Size GetRgbImageSize();
		cv::Point2f DeprojectPoint(cv::Point2f * rgbPoint, void * depthFrame);
		void Release();
		float * GetCorners(const cv::Mat & depthImage);
	};
}


