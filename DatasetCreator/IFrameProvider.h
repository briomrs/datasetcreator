#pragma once
#include <opencv2/opencv.hpp>

namespace CV_CORE
{
	class IFrameProvider
	{
	public:
		virtual ~IFrameProvider() {};
		virtual bool Update() = 0;
		virtual cv::Mat GetRGBFrame() = 0;
		virtual cv::Mat GetLeftInfrFrame() = 0;
		virtual cv::Mat GetRightInfrFrame() = 0;
		virtual cv::Mat GetNativeDepthFrame() = 0;
		virtual void Release() = 0;
	};
}