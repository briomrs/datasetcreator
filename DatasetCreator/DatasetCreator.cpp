﻿#include <iostream>
#include "CameraManager.h"
#include "DepthMapGenerator.h"

using namespace CV_CORE;

void Loop(CameraManager & cm);

int main(int argc, char* argv[])
{
	const char * leftImageFilePath = argv[1];
	const char * rightImageFilePath = argv[2];
	const char * rsDepthMapFilePath = argv[3];
	const char * ourDepthMapFilePath = argv[5];

	Utils::SetParametersStorage("d:/Projects/C++TestApps/DatasetCreator/x64/Release/");

	CameraManager cm(CAMERATYPE::REALSENSE, false);
	float bl = Utils::GetParameterValue<float>(CV_CORE::BASE_LINE);
	//DepthMapGenerator dm(cm.GetQMatrix(), bl);

	Loop(cm);

	return 1;
}

void Loop(CameraManager & cm)
{
	while (true)
	{
		cm.Update();
		cv::Mat mBGRImage = cm.GetImage(IMAGE_KIND::SOURCE_COLOR);
		cv::Mat mLInfrImage = cm.GetImage(IMAGE_KIND::LEFT_INFR);
		cv::Mat mRInfrImage = cm.GetImage(IMAGE_KIND::RIGHT_INFR);
		cv::Mat mNativeDepthImage = cm.GetImage(IMAGE_KIND::DEPTH); //Get depth from frame provider 	

		cv::imshow("RGB", mBGRImage);
		cv::waitKey(1);
	}
}
