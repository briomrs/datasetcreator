#include "RSFrameProvider.h"

namespace CV_CORE
{
	RSFrameProvider::RSFrameProvider(cv::Size infrSize, cv::Size rgbSize, int fps, bool useAlign)
		: FrameProvider(infrSize, rgbSize, fps, useAlign)
	{
		try
		{
			mConfig.enable_stream(RS2_STREAM_INFRARED, 1, mInfrSize.width, mInfrSize.height, RS2_FORMAT_Y8, mFps);
			mConfig.enable_stream(RS2_STREAM_INFRARED, 2, mInfrSize.width, mInfrSize.height, RS2_FORMAT_Y8, mFps);
			mConfig.enable_stream(RS2_STREAM_COLOR, mRGBSize.width, mRGBSize.height, RS2_FORMAT_BGR8, mFps);
			mConfig.enable_stream(RS2_STREAM_DEPTH, mInfrSize.width, mInfrSize.height, RS2_FORMAT_Z16, mFps);

			// start pipeline	
			rs2::pipeline_profile pipeline_profile = mPipeline.start(mConfig);
			rs2::sensor sensor = pipeline_profile.get_device().first<rs2::sensor>();

			if (mUseAlign)
				mAlignToColor = new rs2::align(RS2_STREAM_COLOR);

			auto depth_sensor = pipeline_profile.get_device().first<rs2::depth_sensor>();
			if (depth_sensor.supports(RS2_OPTION_EMITTER_ENABLED))
			{
				depth_sensor.set_option(RS2_OPTION_EMITTER_ENABLED, 0.f); // Disable emitter
			}
			if (depth_sensor.supports(RS2_OPTION_LASER_POWER))
			{
				depth_sensor.set_option(RS2_OPTION_LASER_POWER, 0.f); // Disable laser
			}

			mInitializedWell = true;
		}
		catch (const std::exception&)
		{
			mInitializedWell = false;		
		}
	}

	RSFrameProvider::~RSFrameProvider()
	{
	}

	bool RSFrameProvider::Update()
	{
		if (!mInitializedWell)
		{
			return false;
		}	

		try
		{
			mFrameset = mPipeline.wait_for_frames();
		}
		catch (const std::exception&)
		{
			return false;
		}
		
		if (mUseAlign)
		{
			mFrameset = mAlignToColor->process(mFrameset);
		}
		return true;
	}

	cv::Mat RSFrameProvider::GetRGBFrame()
	{
		if (!mInitializedWell)
			return cv::Mat();

		try
		{
			rs2::frame colorFrame = mFrameset.get_color_frame();
			cv::Mat colorSrc = cv::Mat(mRGBSize, CV_8UC3, (void*)colorFrame.get_data());
			return colorSrc.clone();
		}
		catch (const std::exception&)
		{
			return HandleFrameError();
		}
	}

	cv::Mat RSFrameProvider::GetLeftInfrFrame()
	{
		if (!mInitializedWell)
			return cv::Mat();

		try
		{
			rs2::video_frame leftFrame = mFrameset.get_infrared_frame(1);
			cv::Mat leftSrc = cv::Mat(mInfrSize, CV_8UC1, (void*)leftFrame.get_data());
			return leftSrc.clone();
		}
		catch (const std::exception&)
		{
			return HandleFrameError();
		}

	}

	cv::Mat RSFrameProvider::GetRightInfrFrame()
	{
		if (!mInitializedWell)
			return cv::Mat();

		try
		{
			rs2::video_frame rightFrame = mFrameset.get_infrared_frame(2);
			cv::Mat rightSrc = cv::Mat(mInfrSize, CV_8UC1, (void*)rightFrame.get_data());
			return rightSrc.clone();
		}
		catch (const std::exception&)
		{
			return HandleFrameError();
		}

	}

	cv::Mat RSFrameProvider::GetNativeDepthFrame()
	{
		if (!mInitializedWell)
			return cv::Mat();

		try
		{
			rs2::frame depthFrame = mFrameset.get_depth_frame();

			cv::Size depthImageSize = mUseAlign ? mRGBSize : mInfrSize;
			cv::Mat depthSrc(depthImageSize, CV_16U, (uchar*)depthFrame.get_data(), cv::Mat::AUTO_STEP);
			return depthSrc.clone();			
		}
		catch (const std::exception&)
		{
			return HandleFrameError();
		}
	}

	void RSFrameProvider::Release()
	{
		if (!mInitializedWell)
			return;
		mPipeline.stop();
	}
}


