#include "SBMDisparityCalculator.h"

namespace CV_CORE
{
	SBMDisparityCalculator::SBMDisparityCalculator(int numDisparities, int minBlockSize)
		: DisparityCalculator(numDisparities, minBlockSize)
	{
		mSBM = cv::StereoBM::create(mNumDisparities, mMinBlockSize);
		mSBM.get()->setNumDisparities(mNumDisparities);
		mSBM.get()->setPreFilterSize(5);
		mSBM.get()->setPreFilterCap(61);
		mSBM.get()->setMinDisparity(0);
		mSBM.get()->setTextureThreshold(507);
		mSBM.get()->setUniquenessRatio(0);
		mSBM.get()->setSpeckleWindowSize(0);
		mSBM.get()->setSpeckleRange(1);
	}

	SBMDisparityCalculator::~SBMDisparityCalculator()
	{
	}

	cv::Mat SBMDisparityCalculator::GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage)
	{
		cv::Mat imgDisparity32F = cv::Mat(leftImage.rows, leftImage.cols, CV_32F);
		mSBM->compute(leftImage, rightImage, imgDisparity32F);
		imgDisparity32F.convertTo(imgDisparity32F, CV_32F, scaleCoef);
		return imgDisparity32F;
	}
}



