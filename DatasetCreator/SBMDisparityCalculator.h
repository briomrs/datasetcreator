#pragma once
#include "DisparityCalculator.h"

namespace CV_CORE
{
	class SBMDisparityCalculator : public DisparityCalculator
	{
	private:
		cv::Ptr<cv::StereoBM> mSBM;
	public:
		SBMDisparityCalculator(int numDisparities, int minBlockSize);
		~SBMDisparityCalculator();
		cv::Mat GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage);
	};
}