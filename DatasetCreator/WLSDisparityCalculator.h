#pragma once
#include "DisparityCalculator.h"
#include <opencv2/ximgproc.hpp>
#include "Utils.h"

namespace CV_CORE
{
	class WLSDisparityCalculator : public DisparityCalculator
	{
	private:
		cv::Ptr<cv::ximgproc::DisparityWLSFilter> mWlsFilter;
		cv::Ptr<cv::StereoBM> mLeftMatcher;
		cv::Ptr<cv::StereoMatcher> mRightMatcher;
	public:
		WLSDisparityCalculator(int numDisparities, int minBlockSize);
		~WLSDisparityCalculator();
		cv::Mat GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage);
	};
}