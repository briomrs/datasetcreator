#pragma once
#include <opencv2/opencv.hpp>

namespace CV_CORE
{
	enum DISPARITY_CALCULATOR_TYPE { DC_SBM, DC_SGBM, DC_WLSFILTER, DC_WLSFILTER_GPU };

	class DisparityCalculator
	{
	protected:
		int mNumDisparities;
		int mMinBlockSize;
		DisparityCalculator() {};
		const float scaleCoef = 1.0f / 16.0f;
	public:
		DisparityCalculator(int numDisparities, int minBlockSize)
		{
			mNumDisparities = numDisparities;
			mMinBlockSize = minBlockSize;
		};
		virtual ~DisparityCalculator() {};
		virtual cv::Mat GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage) = 0;
	};
}
