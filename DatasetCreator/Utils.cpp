#include "Utils.h"

namespace CV_CORE
{
	std::string Utils::mStoragePath;
	cv::FileStorage Utils::mSettingsStorage;
	double Utils::mElapsedTime;
	bool Utils::mIsConsoleOutputOn = false;
	bool Utils::mIsFileOutputOn = false;
	bool Utils::mIsConsoleOutputOnchecked = false;
	bool Utils::mIsFileOutputOnchecked = false;
	const char * Utils::mLogFileName = "";
	std::string Utils::mStorageFolderPath = "";
	std::string Utils::mConfigFileName = "/config.yaml";
	LOGGER_TYPE Utils::mLoggerType = LOGGER_TYPE::LOGGER_NONE;
	FILE * Utils::mLogStream;

	std::string Utils::GetParamName(CVCORE_OPTIONS camOpt)
	{
		switch (camOpt)
		{
		case CV_CORE::LI_FX:
			return "LICamera.fx";
		case CV_CORE::LI_FY:
			return "LICamera.fy";
		case CV_CORE::LI_CX:
			return "LICamera.cx";
		case CV_CORE::LI_CY:
			return "LICamera.cy";
		case CV_CORE::LI_K1:
			return "LICamera.k1";
		case CV_CORE::LI_K2:
			return "LICamera.k2";
		case CV_CORE::LI_K3:
			return "LICamera.k3";
		case CV_CORE::LI_K4:
			return "LICamera.k4";
		case CV_CORE::LI_K5:
			return "LICamera.k5";
		case CV_CORE::RI_FX:
			return "RICamera.fx";
		case CV_CORE::RI_FY:
			return "RICamera.fy";
		case CV_CORE::RI_CX:
			return "RICamera.cx";
		case CV_CORE::RI_CY:
			return "RICamera.cy";
		case CV_CORE::RI_K1:
			return "RICamera.k1";
		case CV_CORE::RI_K2:
			return "RICamera.k2";
		case CV_CORE::RI_K3:
			return "RICamera.k3";
		case CV_CORE::RI_K4:
			return "RICamera.k4";
		case CV_CORE::RI_K5:
			return "RICamera.k5";
		case CV_CORE::RGB_FX:
			return "RGBCamera.fx";
		case CV_CORE::RGB_FY:
			return "RGBCamera.fy";
		case CV_CORE::RGB_CX:
			return "RGBCamera.cx";
		case CV_CORE::RGB_CY:
			return "RGBCamera.cy";
		case CV_CORE::RGB_K1:
			return "RGBCamera.k1";
		case CV_CORE::RGB_K2:
			return "RGBCamera.k2";
		case CV_CORE::RGB_K3:
			return "RGBCamera.k3";
		case CV_CORE::RGB_K4:
			return "RGBCamera.k4";
		case CV_CORE::RGB_K5:
			return "RGBCamera.k5";
		case CV_CORE::INFR_H:
			return "LICamera.height";
		case CV_CORE::INFR_W:
			return "LICamera.width";
		case CV_CORE::FPS:
			return "Camera.fps";
		case CV_CORE::EXTRD2C_R1:
			return "ExtrLI2RGB.r1";
		case CV_CORE::EXTRD2C_R2:
			return "ExtrLI2RGB.r2";
		case CV_CORE::EXTRD2C_R3:
			return "ExtrLI2RGB.r3";
		case CV_CORE::EXTRD2C_R4:
			return "ExtrLI2RGB.r4";
		case CV_CORE::EXTRD2C_R5:
			return "ExtrLI2RGB.r5";
		case CV_CORE::EXTRD2C_R6:
			return "ExtrLI2RGB.r6";
		case CV_CORE::EXTRD2C_R7:
			return "ExtrLI2RGB.r7";
		case CV_CORE::EXTRD2C_R8:
			return "ExtrLI2RGB.r8";
		case CV_CORE::EXTRD2C_R9:
			return "ExtrLI2RGB.r9";
		case CV_CORE::EXTRD2C_T1:
			return "ExtrLI2RGB.t1";
		case CV_CORE::EXTRD2C_T2:
			return "ExtrLI2RGB.t2";
		case CV_CORE::EXTRD2C_T3:
			return "ExtrLI2RGB.t3";
		case CV_CORE::EXTRC2D_R1:
			return "ExtrRGB2LI.r1";
		case CV_CORE::EXTRC2D_R2:
			return "ExtrRGB2LI.r2";
		case CV_CORE::EXTRC2D_R3:
			return "ExtrRGB2LI.r3";
		case CV_CORE::EXTRC2D_R4:
			return "ExtrRGB2LI.r4";
		case CV_CORE::EXTRC2D_R5:
			return "ExtrRGB2LI.r5";
		case CV_CORE::EXTRC2D_R6:
			return "ExtrRGB2LI.r6";
		case CV_CORE::EXTRC2D_R7:
			return "ExtrRGB2LI.r7";
		case CV_CORE::EXTRC2D_R8:
			return "ExtrRGB2LI.r8";
		case CV_CORE::EXTRC2D_R9:
			return "ExtrRGB2LI.r9";
		case CV_CORE::EXTRC2D_T1:
			return "ExtrRGB2LI.t1";
		case CV_CORE::EXTRC2D_T2:
			return "ExtrRGB2LI.t2";
		case CV_CORE::EXTRC2D_T3:
			return "ExtrRGB2LI.t3";
		case CV_CORE::EXTRLR_T1:
			return "ExtrLI2RI.t1";
		case CV_CORE::EXTRLR_T2:
			return "ExtrLI2RI.t2";
		case CV_CORE::EXTRLR_T3:
			return "ExtrLI2RI.t3";
		case CV_CORE::RGB_W:
			return "RGBCamera.width";
		case CV_CORE::RGB_H:
			return "RGBCamera.height";
		case CV_CORE::IMU_TYPE:
			return "ImuType";
		case CV_CORE::IMU_AUTO_DETECT:
			return "ImuAutoDetect";
		case CV_CORE::IMU_COM:
			return "ImuComPort";
		case CV_CORE::IMU_TIME_OFFSET:
			return "ImuTimeOffset";
		case CV_CORE::USE_KALMAN:
			return "UseKalmanFiltration";
		case CV_CORE::USE_VIEWER:
			return "Viewer.Enable";
		case CV_CORE::CONSOLE_LOG_SYSTEM:
			return "ConsoleLogSystem";
		case CV_CORE::FILE_LOG_SYSTEM:
			return "FileLogSystem";
		case CV_CORE::LOG_FILE_NAME:
			return "LogFileName";
		case CV_CORE::ARUCO_MARKER_SIZE:
			return "ArucoMarkerSize";
		case CV_CORE::USE_NATIVE_DEPTHMAP:
			return "UseNativeDepthMap";
		case CV_CORE::MAX_AVERAGE_COUNT:
			return "MaxAverageCount";
		case CV_CORE::BASE_LINE:
			return "Camera.bf";
		default:
			return "null";
		}
	}

	bool Utils::IsConsoleOutputOn()
	{
		if (!mIsConsoleOutputOnchecked)
		{
			int t = GetParameterValue<int>(CONSOLE_LOG_SYSTEM);
			mIsConsoleOutputOn = (t > 0) ? true : false;
			std::string s = mIsConsoleOutputOn ? "on" : "off";
			std::cout << "Console LogSytem is " << s << std::endl;
			mIsConsoleOutputOnchecked = true;
		}
		return mIsConsoleOutputOn;
	}

	bool Utils::IsFileOutputOn()
	{
		if (!mIsFileOutputOnchecked)
		{
			int t = GetParameterValue<int>(FILE_LOG_SYSTEM);
			mLoggerType = static_cast<LOGGER_TYPE>(t);
			mIsFileOutputOn = (t > 0) ? true : false;
			mIsFileOutputOnchecked = true;
			if (mIsFileOutputOn)
			{
				std::string fs = mSettingsStorage[GetParamName(LOG_FILE_NAME)];
				mLogFileName = fs.c_str();
				std::string path = mStorageFolderPath + mLogFileName;

				if (mLogFileName == "")
				{
					std::cout << "Log file name is undefined" << std::endl;
					mIsFileOutputOn = false;
					return mIsFileOutputOn;
				}
				InitFileLogging(path.c_str());
			}
			std::string s = mIsFileOutputOn ? "on" : "off";
			std::cout << "File LogSytem is " << s << std::endl;
			
		}
		return mIsFileOutputOn;
	}

	void Utils::InitFileLogging(const char * filePath)
	{
		switch (mLoggerType)
		{
		case CV_CORE::LOGGER_OWN:
			using namespace std;
			freopen_s(&mLogStream, filePath, "w", stdout);
			freopen_s(&mLogStream, filePath, "w", stderr);
			break;
		case CV_CORE::LOGGER_BOOST:
			break;
		case CV_CORE::LOGGER_NONE:
		default:
			return;
			break;
		}
	}

	Utils::Utils()
	{
	}


	Utils::~Utils()
	{
	}

	void Utils::Tic()
	{
		mElapsedTime = (double)cv::getTickCount();
	}

	// toc is called to stop the timer
	void Utils::Toc(const char * message)
	{
		double time = ((double)cv::getTickCount() - mElapsedTime) / cv::getTickFrequency();
		Log(message, std::to_string(time).c_str());
	}

	void Utils::Log(const char * message)
	{
		if (IsConsoleOutputOn())
		{
			std::cout << message << std::endl;
		}		
	}

	void Utils::Log(const char * firstPart, const char * secondPart)
	{
		if (IsConsoleOutputOn())
		{
			std::cout << firstPart << " " << secondPart << std::endl;
		}		
	}

	void Utils::LogToFile(const char * message, LOG_MESSAGE_TYPE type)
	{		
		if (IsFileOutputOn())
		{
			if (mLoggerType == LOGGER_OWN)
			{
				std::cout << message << std::endl;
			}
			else if (mLoggerType == LOGGER_BOOST)
			{
			}
		}
	}

	void Utils::SetParametersStorage(const char * path)
	{		
		mStorageFolderPath = path;
		mStoragePath = std::string(path) + mConfigFileName;
		try
		{
			mSettingsStorage.open(mStoragePath, cv::FileStorage::READ);
		}
		catch (cv::Exception& e)
		{
			
		}

	}
}