#pragma once

namespace CV_CORE
{
	enum CAMERATYPE { REALSENSE };
	enum IMAGE_KIND { SOURCE_COLOR, LEFT_INFR, RIGHT_INFR, DEPTH };
	enum CVCORE_OPTIONS {
		LI_FX, LI_FY, LI_CX, LI_CY, LI_K1, LI_K2, LI_K3, LI_K4, LI_K5,
		RI_FX, RI_FY, RI_CX, RI_CY, RI_K1, RI_K2, RI_K3, RI_K4, RI_K5,
		RGB_FX, RGB_FY, RGB_CX, RGB_CY, RGB_K1, RGB_K2, RGB_K3, RGB_K4, RGB_K5,
		EXTRD2C_R1, EXTRD2C_R2, EXTRD2C_R3, EXTRD2C_R4, EXTRD2C_R5, EXTRD2C_R6, EXTRD2C_R7, EXTRD2C_R8, EXTRD2C_R9, EXTRD2C_T1, EXTRD2C_T2, EXTRD2C_T3,
		EXTRC2D_R1, EXTRC2D_R2, EXTRC2D_R3, EXTRC2D_R4, EXTRC2D_R5, EXTRC2D_R6, EXTRC2D_R7, EXTRC2D_R8, EXTRC2D_R9, EXTRC2D_T1, EXTRC2D_T2, EXTRC2D_T3,
		EXTRLR_T1, EXTRLR_T2, EXTRLR_T3,
		INFR_W, INFR_H, FPS, RGB_W, RGB_H,
		IMU_TYPE, IMU_AUTO_DETECT, IMU_COM, IMU_TIME_OFFSET, USE_KALMAN, USE_VIEWER, CONSOLE_LOG_SYSTEM, FILE_LOG_SYSTEM, LOG_FILE_NAME, ARUCO_MARKER_SIZE, 
		USE_NATIVE_DEPTHMAP, MAX_AVERAGE_COUNT, BASE_LINE
	};
	enum ROTATION_SENSOR_KIND { RS_NGIMU, RS_XSENSE };
	enum CVCORE_ERRORS
	{
		NO_ERRORS = 0,					//No errors
		EMPTY_CALLBACK,					//Callback is empty
		WRONG_FILE_PATH,				//File path is wrong or empty
		FILESTORAGE_CRASH,				//Config file storage was crashed at openning		
		CAMERA_ARE_NOT_CONECTED,		//Camera are not connected or realsense2.dll cannot be find
		COM_PORT_CANNOT_OPEN,			//Unable to open serial connection (possibly serial port is not available)
		UNABLE_SET_TIMEOUTS,			//Unable to set serial port timeouts
		UNABLE_GET_COM_PORT_PARAMS,		//Unable to get serial port parameters
		UNABLE_SET_COM_PORT_PARAMS,		//Unable to set serial port parameters
		FAILED_CREATE_COM_PORT_EVENT,	//Failed create event of com port messages
		SLAM_POSITIONING_ERROR,			//Internal SLAM error
		UNABLE_INIT_SLAM,				//Unable to initialize SLAM object (Internal SLAM error) 
		GRAB_FRAME_PROBLEM,				//Problem during grabbing frame
		OSC_ADRESS_NOT_RECOGNIZED,		//OSC address pattern not recognised
		OSC_ERROR,						//General OSC error
		MULTIPLE_REALESE,				//Release function was already runned 
		CANNOT_SEND_NGIMU_COMAND,		//Ngimu command can't be sended
		WRONG_ARUCO_MARKER_SIZE			//Aruco marker size parameter has wrong value
	};
	enum LOG_MESSAGE_TYPE {M_DEBUG, M_INFO, M_WARNING, M_ERROR, M_FATAL};
	enum LOGGER_TYPE {LOGGER_NONE, LOGGER_OWN, LOGGER_BOOST};

	typedef struct {
		float w;
		float x;
		float y;
		float z;
	} Quaternion;

	typedef struct {
		float roll;
		float pitch;
		float yaw;
	} Euler;

	typedef struct
	{
		float x;
		float y;
		float z;
	} Position;

	typedef struct {
		uchar id;
		Euler euler;
		Position position;
	}MarkerData;

	typedef struct {
		void * rgbImageData;
		void * depthImageData;
		void * slamMatrix;
		Quaternion rotationData;
		double timestamp;
		uint markersCount;
		void * arucoMarkers;
		float * corners;
	} DataPacket;
}