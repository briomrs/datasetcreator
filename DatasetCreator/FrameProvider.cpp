#include "FrameProvider.h"

namespace CV_CORE
{
	cv::Mat FrameProvider::HandleFrameError()
	{
		return cv::Mat::zeros(mRGBSize, CV_8UC3);
	}

	FrameProvider::FrameProvider(cv::Size infrSize, cv::Size rgbSize, int fps, bool useAlign)
	{
		mInfrSize = infrSize;
		mRGBSize = rgbSize;
		mFps = fps;
		mUseAlign = useAlign;
	}

	FrameProvider::~FrameProvider()
	{
	}
}
