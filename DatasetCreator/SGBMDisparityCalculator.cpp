#include "SGBMDisparityCalculator.h"

namespace CV_CORE
{
	SGBMDisparityCalculator::SGBMDisparityCalculator(int numDisparities, int minBlockSize)
		: DisparityCalculator(numDisparities, minBlockSize)
	{
		sgbm = cv::StereoSGBM::create(0, mNumDisparities, mMinBlockSize);
		sgbm.get()->setNumDisparities(mNumDisparities);
		sgbm.get()->setPreFilterCap(61);
		sgbm.get()->setMinDisparity(0);
		sgbm.get()->setUniquenessRatio(0);
		sgbm.get()->setSpeckleWindowSize(0);
		sgbm.get()->setSpeckleRange(1);
	}

	SGBMDisparityCalculator::~SGBMDisparityCalculator()
	{
	}

	cv::Mat SGBMDisparityCalculator::GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage)
	{
		cv::Mat imgDisparity32F = cv::Mat(leftImage.rows, leftImage.cols, CV_32F);
		sgbm->setMode(cv::StereoSGBM::MODE_SGBM_3WAY);
		sgbm->compute(leftImage, rightImage, imgDisparity32F);
		imgDisparity32F.convertTo(imgDisparity32F, CV_32F, scaleCoef);
		return imgDisparity32F;
	}
}
