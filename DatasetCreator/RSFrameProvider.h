#pragma once
#include "FrameProvider.h"
#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>

namespace CV_CORE
{
	class RSFrameProvider : public FrameProvider
	{
	private:
		rs2::config mConfig;
		rs2::pipeline mPipeline;
		rs2::frameset mFrameset;

		rs2::align * mAlignToColor;
	public:
		RSFrameProvider(cv::Size infrSize, cv::Size rgbSize, int fps, bool useAlign);
		~RSFrameProvider();

		bool Update();
		cv::Mat GetRGBFrame();
		cv::Mat GetLeftInfrFrame();
		cv::Mat GetRightInfrFrame();
		cv::Mat GetNativeDepthFrame();
		void Release();
	};
}

