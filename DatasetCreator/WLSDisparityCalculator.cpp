#include "WLSDisparityCalculator.h"

namespace CV_CORE
{
	WLSDisparityCalculator::WLSDisparityCalculator(int numDisparities, int minBlockSize)
		: DisparityCalculator(numDisparities, minBlockSize)
	{
		mLeftMatcher = cv::StereoBM::create(numDisparities, minBlockSize);
		mWlsFilter = cv::ximgproc::createDisparityWLSFilter(mLeftMatcher);
		mRightMatcher = cv::ximgproc::createRightMatcher(mLeftMatcher);
	}

	WLSDisparityCalculator::~WLSDisparityCalculator()
	{
	}

	cv::Mat WLSDisparityCalculator::GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage)
	{
		cv::Mat imgDisparity32F = cv::Mat(leftImage.rows, leftImage.cols, CV_32F);
		cv::Mat leftDisp;
		cv::Mat rightDisp;
		cv::Mat filteredDisp;

		mLeftMatcher->compute(leftImage, rightImage, leftDisp);
		mRightMatcher->compute(rightImage, leftImage, rightDisp);

		mWlsFilter->setLambda(8000.0);
		mWlsFilter->setSigmaColor(1.5);

		mWlsFilter->filter(leftDisp, leftImage, filteredDisp, rightDisp);

		filteredDisp.convertTo(filteredDisp, CV_32F, scaleCoef);

		imgDisparity32F = filteredDisp.clone();
		return imgDisparity32F;
	}
}


