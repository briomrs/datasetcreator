#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include "DisparityCalculator.h"
#include "SBMDisparityCalculator.h"
#include "SGBMDisparityCalculator.h"
#include "WLSDisparityCalculator.h"
#include "Utils.h"

namespace CV_CORE
{
	class DepthMapGenerator
	{
	private:
		cv::Size mImageSize;
		cv::Size mAlignedSize;
		const int mNumDisparities = 32; // The number of disparities in depth map. This number affects on effective width of depth map. This value was selected empirically.
		const int mMinBlockSize = 15;
		const DISPARITY_CALCULATOR_TYPE mDCType = DC_WLSFILTER;
		DisparityCalculator * mDisparityCalculator;
		cv::Rect boundariesRect;

		cv::Mat getDisparityMap(const cv::Mat &cameraImageLeft, const cv::Mat &cameraImageRight);
		cv::Mat mDisparityMap;

	public:
		DepthMapGenerator(cv::Mat QMatrix, float bl);
		~DepthMapGenerator();

		cv::Mat GetDepthMap(const cv::Mat & leftImage, const cv::Mat & rightImage);
		//cv::Mat GetDepthMapGpu(const cv::Mat & leftImage, const cv::Mat & rightImage);
		cv::Mat GetDepthMapPoint3d(const cv::Mat & leftImage, const cv::Mat & rightImage);
		cv::Mat GetAlignedDepthMap();
		void Release();

		cv::Point3f GetPointCoordinate3d(cv::Point2f * imagePoint);
		void SetAlignParameters(const cv::Mat & DepthToRgbTransform, const cv::Mat & RgbCameraMatrix, const cv::Size & alignSize);
	};
}

