#include "CameraManager.h"

namespace CV_CORE
{
	void CameraManager::InitCameraParams()
	{
		DefineCamera();
		DefineCameraMatricies();
		StereoRectify();
	}

	void CameraManager::DefineCamera()
	{
		switch (mCameraType)
		{
		case CV_CORE::REALSENSE:
			mFrameProvider = new RSFrameProvider(mInfrSize, mRGBSize, mFps, mUseAlign);
			break;
		default:
			break;
		}
	}

	void CameraManager::DefineCameraMatricies()
	{
		//Left infrated camera parameters such as fx, fy, cx, cy and distirtion coeffs.
		float lfx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_FX);
		float lfy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_FY);
		float lcx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_CX);
		float lcy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_CY);
		float lk1 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_K1);
		float lk2 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_K2);
		float lk3 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_K3);
		float lk4 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_K3);
		float lk5 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::LI_K5);
		
		//Right infrated camera parameters such as fx, fy, cx, cy and distirtion coeffs.
		float rfx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_FX);
		float rfy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_FY);
		float rcx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_CX);
		float rcy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_CY);
		float rk1 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_K1);
		float rk2 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_K2);
		float rk3 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_K3);
		float rk4 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_K3);
		float rk5 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RI_K5);
		
		//Translation vector of extrinsic from left infrated to right infrated cameras
		float extrLI2RIT1 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRLR_T1);
		float extrLI2RIT2 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRLR_T2);
		float extrLI2RIT3 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRLR_T3);
		
		mLeftCameraMatrix = (cv::Mat_<double>(3, 3) << lfx, 0, lcx, 0, lfy, lcy, 0, 0, 1);
		mRightCameraMatrix = (cv::Mat_<double>(3, 3) << rfx, 0, rcx, 0, rfy, rcy, 0, 0, 1);
		//In default case infrated cameras a not rotate belong each other
		mMutualRotationMatrix = (cv::Mat_<double>(3, 3) << 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
		mMutualTransformMatrix = (cv::Mat_<double>(3, 1) << extrLI2RIT1, extrLI2RIT2, extrLI2RIT3);

		mDepthIntrinsics.width = mInfrSize.width;
		mDepthIntrinsics.height = mInfrSize.height;
		mDepthIntrinsics.fx = lfx;
		mDepthIntrinsics.fy = lfy;
		mDepthIntrinsics.model = RS2_DISTORTION_BROWN_CONRADY;
		mDepthIntrinsics.ppx = lcx;
		mDepthIntrinsics.ppy = lcy;
		mDepthIntrinsics.coeffs[0] = lk1;
		mDepthIntrinsics.coeffs[1] = lk2;
		mDepthIntrinsics.coeffs[2] = lk3;
		mDepthIntrinsics.coeffs[3] = lk4;
		mDepthIntrinsics.coeffs[4] = lk5;

		mColorIntrinsics.width = mRGBSize.width;
		mColorIntrinsics.height = mRGBSize.height;

		float cfx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_FX);
		float cfy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_FY);
		float ccx = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_CX);
		float ccy = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_CY);
		float ck1 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_K1);
		float ck2 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_K2);
		float ck3 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_K3);
		float ck4 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_K4);
		float ck5 = Utils::GetParameterValue<float>(CVCORE_OPTIONS::RGB_K5);

		mColorIntrinsics.fx = cfx;
		mColorIntrinsics.fy = cfy;
		mColorIntrinsics.model = RS2_DISTORTION_BROWN_CONRADY;
		mColorIntrinsics.ppx = ccx;
		mColorIntrinsics.ppy = ccy;
		mColorIntrinsics.coeffs[0] = ck1;
		mColorIntrinsics.coeffs[1] = ck2;
		mColorIntrinsics.coeffs[2] = ck3;
		mColorIntrinsics.coeffs[3] = ck4;
		mColorIntrinsics.coeffs[4] = ck5;

		mDepthCameraMatrix = (cv::Mat_<double>(3, 3) << lfx, 0, lcx, 0, lfy, lcy, 0, 0, 1);
		mRgbCameraMatrix = (cv::Mat_<double>(3, 3) << cfx, 0, ccx, 0, cfy, ccy, 0, 0, 1);
		mRgbDistortionMatrix = (cv::Mat_<double>(1, 3) << ck1, ck2, ck3);

		mDepthToColorExtrinsics.rotation[0] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R1);
		mDepthToColorExtrinsics.rotation[1] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R2);
		mDepthToColorExtrinsics.rotation[2] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R3);
		mDepthToColorExtrinsics.rotation[3] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R4);
		mDepthToColorExtrinsics.rotation[4] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R5);
		mDepthToColorExtrinsics.rotation[5] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R6);
		mDepthToColorExtrinsics.rotation[6] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R7);
		mDepthToColorExtrinsics.rotation[7] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R8);
		mDepthToColorExtrinsics.rotation[8] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_R9);
		mDepthToColorExtrinsics.translation[0] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_T1);
		mDepthToColorExtrinsics.translation[1] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_T2);
		mDepthToColorExtrinsics.translation[2] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRD2C_T3);

		mDepthToColorExtrinsicMat = (cv::Mat_<double>(4, 4)<< mDepthToColorExtrinsics.rotation[0], mDepthToColorExtrinsics.rotation[1], mDepthToColorExtrinsics.rotation[2], mDepthToColorExtrinsics.translation[0],
															 mDepthToColorExtrinsics.rotation[3], mDepthToColorExtrinsics.rotation[4], mDepthToColorExtrinsics.rotation[5], mDepthToColorExtrinsics.translation[1], 
															 mDepthToColorExtrinsics.rotation[6], mDepthToColorExtrinsics.rotation[7], mDepthToColorExtrinsics.rotation[8], mDepthToColorExtrinsics.translation[2],
															 0, 0, 0, 1);

		mColorToDepthExtrinsics.rotation[0] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R1);
		mColorToDepthExtrinsics.rotation[1] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R2);
		mColorToDepthExtrinsics.rotation[2] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R3);
		mColorToDepthExtrinsics.rotation[3] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R4);
		mColorToDepthExtrinsics.rotation[4] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R5);
		mColorToDepthExtrinsics.rotation[5] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R6);
		mColorToDepthExtrinsics.rotation[6] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R7);
		mColorToDepthExtrinsics.rotation[7] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R8);
		mColorToDepthExtrinsics.rotation[8] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_R9);
		mColorToDepthExtrinsics.translation[0] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_T1);
		mColorToDepthExtrinsics.translation[1] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_T2);
		mColorToDepthExtrinsics.translation[2] = Utils::GetParameterValue<float>(CVCORE_OPTIONS::EXTRC2D_T3);

		mDepth2RgbRt = (cv::Mat_<double>(4, 4) << mDepthToColorExtrinsics.rotation[0], mDepthToColorExtrinsics.rotation[1], mDepthToColorExtrinsics.rotation[2], mDepthToColorExtrinsics.translation[0],
												  mDepthToColorExtrinsics.rotation[3], mDepthToColorExtrinsics.rotation[4], mDepthToColorExtrinsics.rotation[5], mDepthToColorExtrinsics.translation[1],
												  mDepthToColorExtrinsics.rotation[6], mColorToDepthExtrinsics.rotation[7], mColorToDepthExtrinsics.rotation[8], mColorToDepthExtrinsics.translation[2],
												  0, 0, 0, 1);
	}

	void CameraManager::StereoRectify()
	{
		cv::stereoRectify(mLeftCameraMatrix,
			mLeftDistortionMatrix,
			mRightCameraMatrix,
			mRightDistortionMatrix,
			mInfrSize,
			mMutualRotationMatrix,
			mMutualTransformMatrix,
			R1,
			R2,
			P1,
			P2,
			mQMatrix,
			cv::CALIB_ZERO_DISPARITY,
			-1,
			mInfrSize,
			&roi1,
			&roi2);

		mQMatrix.convertTo(mQMatrix, CV_32F);
	}

	bool CameraManager::Reconnect()
	{
		delete mFrameProvider;
		DefineCamera();
		return mFrameProvider->Update();
	}

	CameraManager::CameraManager(CAMERATYPE cType, bool useAlign)
	{
		mCameraType = cType;
		mUseAlign = useAlign;
		int infrWidth = Utils::GetParameterValue<int>(CVCORE_OPTIONS::INFR_W);
		int infrHeight = Utils::GetParameterValue<int>(CVCORE_OPTIONS::INFR_H);
		mInfrSize = cv::Size(infrWidth, infrHeight);
		
		int rgbWidth = Utils::GetParameterValue<int>(CVCORE_OPTIONS::RGB_W);
		int rgbHeight = Utils::GetParameterValue<int>(CVCORE_OPTIONS::RGB_H);
		mRGBSize = cv::Size(rgbWidth, rgbHeight);
		
		mFps = Utils::GetParameterValue<int>(CVCORE_OPTIONS::FPS);
		InitCameraParams();
	}

	CameraManager::~CameraManager()
	{
	}

	cv::Mat CameraManager::GetQMatrix()
	{
		return mQMatrix;
	}
	cv::Mat CameraManager::GetImage(IMAGE_KIND kind)
	{
		cv::Mat result;
		switch (kind)
		{
		case CV_CORE::SOURCE_COLOR:
			result = mFrameProvider->GetRGBFrame();
			break;
		case CV_CORE::LEFT_INFR:
			result = mFrameProvider->GetLeftInfrFrame();
			break;
		case CV_CORE::RIGHT_INFR:
			result = mFrameProvider->GetRightInfrFrame();
			break;
		case CV_CORE::DEPTH:
			result = mFrameProvider->GetNativeDepthFrame();
			break;
		default:
			break;
		}
		return result;
	}

	void CameraManager::Update()
	{
		bool res = mFrameProvider->Update();
		if (res)
			return;
	
		while (!Reconnect())
		{
			Utils::Log("Try to reconnect camera");
		}
		Utils::Log("Camera reconnection done!");
	}

	cv::Mat CameraManager::GetLeftCameraMatrix()
	{
		return mLeftCameraMatrix;
	}

	cv::Mat CameraManager::GetRightCameraMatrix()
	{
		return mRightCameraMatrix;
	}
	cv::Mat CameraManager::GetMutualRotationMatrix()
	{
		return mMutualRotationMatrix;
	}

	cv::Mat CameraManager::GetMutualTransformMatrix()
	{
		return mMutualTransformMatrix;
	}

	cv::Mat CameraManager::GetLeftDistortionMatrix()
	{
		return mLeftDistortionMatrix;
	}

	cv::Mat CameraManager::GetRightDistortionMatrix()
	{
		return mRightDistortionMatrix;
	}

	cv::Mat CameraManager::GetDepth2RgbExtrinsic()
	{
		return mDepthToColorExtrinsicMat;
	}

	cv::Mat CameraManager::GetRgbCameraMatrix()
	{
		return mRgbCameraMatrix;
	}

	cv::Mat CameraManager::GetAlignedDepth(const cv::Mat & depthImage)
	{
		cv::Mat result = cv::Mat::zeros(mRGBSize, CV_32F);
		if (!depthImage.empty())
		{
			//Rgb image boundary points
			cv::Point2f rgbPointTL = cv::Point(0, 0);
			cv::Point2f rgbPointBR = cv::Point(mRGBSize.width, mRGBSize.height);

			//Corresponding depth boundary points
			cv::Point2f p1 = DeprojectPoint(&rgbPointTL, depthImage.data);
			cv::Point2f p2 = DeprojectPoint(&rgbPointBR, depthImage.data);

			//Depth crop rectangle
			cv::Rect2f rect = cv::Rect2f(p1, p2);
			cv::Mat croppedDepth = depthImage(rect);

			//Scale cropped depth
			cv::resize(croppedDepth, result, mRGBSize);
		}
		return result;
	}

	cv::Size CameraManager::GetRgbImageSize()
	{
		return mRGBSize;
	}

	cv::Point2f CameraManager::DeprojectPoint(cv::Point2f * rgbPoint, void * depthFrame)
	{
		float result[2] = { 0 };
		float fromPixel[2] = { rgbPoint->x, mRGBSize.height - rgbPoint->y };
		float depthScale = 0.01f;

		rs2_project_color_pixel_to_depth_pixel(result,
			reinterpret_cast<const uint16_t*>(depthFrame),
			depthScale,
			0.1,
			10,
			&mDepthIntrinsics,
			&mColorIntrinsics,
			&mColorToDepthExtrinsics,
			&mDepthToColorExtrinsics,
			fromPixel);
		cv::Point2f resultPoint = cv::Point2f(result[0], result[1]);
		return resultPoint;
	}

	void CameraManager::Release()
	{
		std::cout << "Release camera manager" << std::endl;
		mFrameProvider->Release();
		std::cout << "Camera manager released, try to delete frame provider" << std::endl;
		delete mFrameProvider;
		std::cout << "mFrameProvider deleted" << std::endl;
	}

	float * CameraManager::GetCorners(const cv::Mat & depthImage)
	{
		if (!mIsCornersCalculated)
		{
			//Rgb image boundary points
			cv::Point2f rgbPointTL = cv::Point(0, 0);
			cv::Point2f rgbPointBR = cv::Point(mRGBSize.width, mRGBSize.height);

			//Corresponding depth boundary points
			cv::Point2f p1 = DeprojectPoint(&rgbPointTL, depthImage.data);
			cv::Point2f p2 = DeprojectPoint(&rgbPointBR, depthImage.data);

			float result[4] = { p1.x, p1.y, p2.x, p2.y };
			mCorners = result;
			mIsCornersCalculated = true;
		}		
		return mCorners;
	}
}



