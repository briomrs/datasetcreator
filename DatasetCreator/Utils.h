#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include <string>
#include "DataStructures.h"
#include <iostream>
#include <boost/move/utility_core.hpp>

namespace CV_CORE
{
	class Utils
	{
	private:
		static FILE * mLogStream;
		//Path to config file
		static std::string mStoragePath;
		//Path to config file folder
		static std::string mStorageFolderPath;
		static cv::FileStorage mSettingsStorage;
		static LOGGER_TYPE mLoggerType;

		static std::string GetParamName(CVCORE_OPTIONS camOpt);
		static bool mIsConsoleOutputOn;
		static bool mIsFileOutputOn;
		static const char * mLogFileName;
		static double mElapsedTime;
		static bool IsConsoleOutputOn();
		static bool IsFileOutputOn();
		static bool mIsConsoleOutputOnchecked;
		static bool mIsFileOutputOnchecked;
		static std::string mConfigFileName;
		static void InitFileLogging(const char * filePath);
	public:
		Utils();
		~Utils();
		static void SetParametersStorage(const char * path);
		static void Tic();
		static void Toc(const char * message);
		static void Log(const char * message);
		static void Log(const char * firstPart, const char * secondPart);
		static void LogToFile(const char * message, LOG_MESSAGE_TYPE type = LOG_MESSAGE_TYPE::M_INFO);

		template <typename T>
		static T GetParameterValue(CVCORE_OPTIONS camOpt)
		{
			std::string parameterId = GetParamName(camOpt);
			if (!mSettingsStorage.isOpened() || parameterId == "null")
			{
				T result = NULL;
				return result;
			}
			T result = mSettingsStorage[parameterId];
			return result;
		}
	};
}
