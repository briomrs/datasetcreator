#pragma once
#include "DisparityCalculator.h"

namespace CV_CORE
{
	class SGBMDisparityCalculator : public DisparityCalculator
	{
	private:
		cv::Ptr<cv::StereoSGBM> sgbm;
	public:
		SGBMDisparityCalculator(int numDisparities, int minBlockSize);
		~SGBMDisparityCalculator();
		cv::Mat GetDisparity(const cv::Mat & leftImage, const cv::Mat & rightImage);
	};
}
