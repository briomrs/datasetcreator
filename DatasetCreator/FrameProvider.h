#pragma once
#include "IFrameProvider.h"
#include "Utils.h"

namespace CV_CORE
{
	class FrameProvider : public IFrameProvider
	{
	protected:
		cv::Size mInfrSize;
		cv::Size mRGBSize;
		int mFps;
		cv::Mat HandleFrameError();

		bool mUseAlign;
		bool mInitializedWell;
	public:
		FrameProvider(cv::Size infrSize, cv::Size rgbSize, int fps, bool useAlign);
		virtual ~FrameProvider();
	};
}
