#include "DepthMapGenerator.h"

namespace CV_CORE
{
	typedef float Pixel;
	cv::Mat resultImage;
	cv::Mat cvPointCloud;
	cv::Mat alignedDepthMap;
	int imageRowWidth;
	Eigen::Matrix4f mQMatrix;
	Eigen::Matrix3f mDepthToColorRotate;
	Eigen::Vector3f mDepthToColorTranslate;
	float fxRgbCam, fyRgbCam, cxRgbCam, cyRgbCam;
	int mAlignedWidth, mAlignedHeight;
	bool mIsAlignParametersSetted = false;
	float baseLine;

	void createDepthMap(Pixel & pixel, const int & x, const int & y)
	{
		resultImage.at<float>(x, y) = baseLine / pixel;
	}

	void createDepthMapOldVersion(Pixel & pixel, const int & x, const int & y)
	{
		Eigen::Vector4f tmp(x, y, pixel, 1);
		Eigen::Vector4f res = mQMatrix * tmp;
		res = res / res(3);
		resultImage.at<float>(x, y) = std::sqrt(res(0) * res(0) + res(1) + res(1) + res(2) * res(2));		
		resultImage.at<float>(x, y) = res(2);
	}

	void createDepthMapPoint3d(Pixel & pixel, const int & x, const int & y)
	{
		Eigen::Vector4f tmp(x, y, pixel, 1);
		Eigen::Vector4f res = mQMatrix * tmp;
		res = res / res(3);
		cvPointCloud.at<cv::Vec3d>(x, y) = cv::Vec3d(res(0), res(1), res(2));
	}

	struct DepthMapCreator
	{
		void operator ()(Pixel &pixel, const int * position) const
		{
			createDepthMap(pixel, position[0], position[1]);
		}
	};

	struct DepthMapCreatorPoint3d
	{
		void operator ()(Pixel &pixel, const int * position) const
		{
			createDepthMapPoint3d(pixel, position[0], position[1]);
		}
	};

	cv::Mat DepthMapGenerator::getDisparityMap(const cv::Mat & cameraImageLeft, const cv::Mat & cameraImageRight)
	{
		return mDisparityCalculator->GetDisparity(cameraImageLeft, cameraImageRight);
	}

	DepthMapGenerator::DepthMapGenerator(cv::Mat QMatrix, float bl)
	{
		int width = Utils::GetParameterValue<int>(INFR_W);
		int height = Utils::GetParameterValue<int>(INFR_H);
		mImageSize = cv::Size(width, height);

		cv::cv2eigen(QMatrix, mQMatrix);

		switch (mDCType)
		{
		case CV_CORE::DC_SBM:
			mDisparityCalculator = new SBMDisparityCalculator(mNumDisparities, mMinBlockSize);
			break;
		case CV_CORE::DC_SGBM:
			mDisparityCalculator = new SGBMDisparityCalculator(mNumDisparities, mMinBlockSize);
			break;
		case CV_CORE::DC_WLSFILTER:
			mDisparityCalculator = new WLSDisparityCalculator(mNumDisparities, mMinBlockSize);
			break;
		default:
			break;
		}

		//Calculate this if you need depth map or point cloud without boundaries.
		int halfMBS = mMinBlockSize * 0.5;
		boundariesRect = cv::Rect(mNumDisparities + halfMBS, halfMBS, (width - mNumDisparities - mMinBlockSize + 1), (height - mMinBlockSize + 1));

		baseLine = bl;
	}

	DepthMapGenerator::~DepthMapGenerator()
	{
	}

	cv::Mat DepthMapGenerator::GetDepthMap(const cv::Mat & leftImage, const cv::Mat &rightImage)
	{
		if (leftImage.empty() || rightImage.empty())
			return cv::Mat();

		mDisparityMap = getDisparityMap(leftImage, rightImage);

		cv::Mat adjMap;
		// expand your range to 0..255. Similar to histEq();
		mDisparityMap.convertTo(adjMap, CV_8UC1, 255/20);

		// this is great. It converts your grayscale image into a tone-mapped one, 
		// much more pleasing for the eye
		// function is found in contrib module, so include contrib.hpp 
		// and link accordingly
		cv::Mat falseColorsMap;
		applyColorMap(adjMap, falseColorsMap, cv::COLORMAP_AUTUMN);
		cv::imshow("Disparity", falseColorsMap);
		cv::waitKey(1);

		resultImage = cv::Mat(leftImage.rows, leftImage.cols, CV_32F);
		mDisparityMap.forEach<Pixel>([](Pixel &pixel, const int * position) -> void
		{
			createDepthMapOldVersion(pixel, position[0], position[1]);
		}
		);
		return resultImage;
	}

	/*cv::Mat DepthMapGenerator::GetDepthMapGpu(const cv::Mat & leftImage, const cv::Mat & rightImage)
	{
		//if (leftImage.empty() || rightImage.empty())
		//	return cv::Mat();
		//
		//mDisparityMap = getDisparityMap(leftImage, rightImage);
		//
		//return GetImageFromGpu(mDisparityMap, mQMatrix);
		return cv::Mat();
	}*/

	cv::Mat DepthMapGenerator::GetDepthMapPoint3d(const cv::Mat & leftImage, const cv::Mat & rightImage)
	{
		mDisparityMap = getDisparityMap(leftImage, rightImage);

		//disparity = cv::Mat(disparity, boundariesRect);

		cvPointCloud = cv::Mat(leftImage.rows, leftImage.cols, CV_64FC3);
		mDisparityMap.forEach<Pixel>([](Pixel &pixel, const int * position) -> void
		{
			createDepthMapPoint3d(pixel, position[0], position[1]);
		}
		);
		return cvPointCloud;
	}

	cv::Mat DepthMapGenerator::GetAlignedDepthMap()
	{
		return alignedDepthMap;
	}

	void DepthMapGenerator::Release()
	{
		delete mDisparityCalculator;
	}

	cv::Point3f DepthMapGenerator::GetPointCoordinate3d(cv::Point2f * imagePoint)
	{
		cv::Point3f resultPoint = cv::Point3f(0, 0, 0);
		if (!mDisparityMap.empty())
		{
			float x = imagePoint->x;
			float y = imagePoint->y;

			if (x < mNumDisparities)			
				x = mNumDisparities + mMinBlockSize; //There is a blind area in the left of the depth image with the lenght of (mNumDisparities + mMinBlockSize/2).

			float pixel = mDisparityMap.at<float>(y, x);
			Eigen::Vector4f tmp(x, y, pixel, 1);
			Eigen::Vector4f res = mQMatrix * tmp;
			res = res / res(3);
			resultPoint = cv::Point3f(res(0), res(1), res(2));
		}
		return resultPoint;
	}

	void DepthMapGenerator::SetAlignParameters(const cv::Mat & depthToRgbTransform, const cv::Mat & rgbCameraMatrix, const cv::Size & alignSize)
	{
		cv::Mat dtcR = depthToRgbTransform.colRange(0, 2).rowRange(0, 2);
		cv::Mat dtcT = depthToRgbTransform.col(3).rowRange(0, 2);
		cv::cv2eigen(dtcT, mDepthToColorTranslate);
		cv::cv2eigen(dtcR, mDepthToColorRotate);
		fxRgbCam = rgbCameraMatrix.at<double>(0, 0);
		fyRgbCam = rgbCameraMatrix.at<double>(1, 1);
		cxRgbCam = rgbCameraMatrix.at<double>(0, 2);
		cyRgbCam = rgbCameraMatrix.at<double>(1, 2);
		mAlignedSize = alignSize;
		mAlignedHeight = mAlignedSize.height;
		mAlignedWidth = mAlignedSize.width;

		mIsAlignParametersSetted = true;
	}
}



